import os
from flask import Flask, request, render_template, jsonify, make_response, flash, redirect, url_for
from werkzeug.utils import secure_filename
import pickle
from DeepImageSearch import Index,LoadData,SearchImage
import pandas as pd
from PIL import Image, ImageDraw, ImageOps, ImageFilter

app = Flask(__name__, static_url_path='/static')

UPLOAD_FOLDER = 'static\\assets\\uploaded-file'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

app.config.update(SECRET_KEY=os.urandom(24))

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def expand2square(pil_img, background_color):
    width, height = pil_img.size
    if width == height:
        return pil_img
    elif width > height:
        result = Image.new(pil_img.mode, (width, width), background_color)
        result.paste(pil_img, (0, (width - height) // 2))
        return result
    else:
        result = Image.new(pil_img.mode, (height, height), background_color)
        result.paste(pil_img, ((height - width) // 2, 0))
        return result

def getDominantColor(pil_img):
    w, h = pil_img.size
    # Set up edge margin to look for dominant color
    me = 3
    
    # On an image copy, set non edge pixels to (0, 0, 0, 0)
    img_copy = pil_img.copy()
    draw = ImageDraw.Draw(img_copy)
    draw.rectangle((me, me, w - (me + 1), h - (me + 1)), (0, 0, 0, 0))
        
    n_colors = sorted(img_copy.getcolors(2 * me * (w + h) + 1), reverse=True)
    dom_color = n_colors[0][1] if n_colors[0][1] != (0, 0, 0, 0) else n_colors[1][1]
    
    return dom_color

@app.route('/')
def index():
    return render_template('index.html', insurance_cost=0)

@app.route('/api/calculate', methods=['GET', 'POST'])
def calculate():
    if request.method == 'POST':
        file = request.files['logoupload']
        
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save('static\\assets\\uploaded-file\\' + filename)

        img = Image.open('static\\assets\\uploaded-file\\' + filename).convert('RGBA')
        img = expand2square(img, getDominantColor(img))
        img.convert('RGB').save('static\\assets\\uploaded-file\\' + filename)

        # return SearchImage().get_similar_images(image_path='static\\assets\\uploaded-file\\' + filename ,number_of_images=18)

        return [sub.replace('logo-similarity-checker-engine\\static\\LOGODIX_AUG', 'LOGODIX') for i, sub in SearchImage().get_similar_images(image_path='static\\assets\\uploaded-file\\' + filename ,number_of_images=18).items()]

    return make_response(jsonify({'success':True}), 201)

if __name__ == '__main__':
    app.config['SESSION_TYPE'] = 'filesystem'
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    sess.init_app(app)
    app.run(debug=True, TEMPLATES_AUTO_RELOAD=True)