# Logo Similarity Checker - Flask Search Engine Deployment

## **Anggota Kelompok:**
1. Irwansyah (10519011)
2. Muhammad Agy Nurwicaksono (10519022)
3. Mugia Miftahul Choir (10519035)

---
## **Repository Description**

Respository ini ditujukkan untuk memenuhi pengumpulan Tugas Proyek Data Mining kelompok **"Logo Similarity Checker"**.

Repository ini berisi:
- `Source Code` Search Engine
- `Pickles File`

***Untuk dataset logo hasil crawling tidak dapat diupload di github karena berjumlah +- 1GB. Silahkan download di-link terpisah yang tercantum pada file word pengumpulan tugas.***

---

## **Sekilas mengenai cara kerja aplikasi**

- Agar dapat memprediksi kemiripan logo dengan dataset yang sudah di training, data input model harus mengikuti format sebagai berikut:
   - Upload file logo yang ingin ditelusuri
   - Format file yang didukung adalah **JPG, JPEG, PNG, dan GIF**
- Setelah file dipilih melalui input file yang tersedia pada website, gambar akan di-upload menggunakan javascript kedalam folder **static/assets/uploaded-file**.
- Lalu, vector pada gambar akan di-extract menggunakan arsitektur **VGG-16** dan **ImageNet** sebagai parameter weight, dengan library keras tensorflow.
- Setelah itu, query vector akan dilakukan pencarian kedalam index **image_features_vectors.ann berjumlah 17.000+ rows** menggunakan **AnnoyIndex euclidean**.
- Hasil dari pencarian akan dilempar kembali ke dalam aplikasi dengan format **JSON**.
- Terakhir, javascript akan melakukan iterasi hasil pencarian, dan menampilkannya pada website.

---
## **Cara menjalankan Aplikasi pada komputer Anda**

### 1. Install Library yang diperllukan
Berikut library yang diperlukan untuk menjalankan aplikasi ini:
- pandas
- DeepImageSearch
- pickle
- flask
- werkzeug.utils

### 2.  Download dataset LOGODIX
1. Silahkan download dataset LOGODIX pada url yang disediakan oleh kami.
1. Copy folder LOGODIX ke dalam folder static/

### 3. Menjalankan Server Flask

<!-- 1. Pastikan Anda sudah menginstall Anaconda
1. Buka terminal/command prompt/power shell
1. Buat virtual environment dengan\
   `conda create -n <nama-environment> python=3.9`
1. Aktifkan virtual environment dengan\
   `conda activate <nama-environment>`
1. Install semua dependency/package Python dengan\
   `pip install -r requirements.txt` -->
1. Jalankan Aplikasi menggunakan perintah\
   `python -m flask run --reload --debugger`

### 4. Akses melalui Website

Setelah Server berjalan:

1. Anda akan diberikan URL untuk membuka website berupa `localhost:5000/` atau `127.0.0.1:5000/`
1. Buka URL dengan browser, coba upload logo yang ingin dilakukan pencarian kemiripannya.
1. Tunggu beberapa saat.
1. Anda akan melihat 18 logo yang memiliki kemiripan dengan logo anda.

---

## **Folder, file, dan kegunaannya**

- meta-data-files/
   - image_data_features.pkl --> Trained model (logo meta information)
   - image_features_vectors.ann --> Index file yang dibuat menggunakan AnnoyIndex
- static/
   - assets --> asset statis berupa image dan folder uploaded-file
   - css --> asset css
   - js --> asset js
   - LOGODIX --> folder dataset, masukkan hasil download LOGODIX disini
- templates/
   - index.html --> Berisi template website
- app.py --> File program inti

---
## **Batasan Aplikasi**
- Aplikasi ini hanya menggunakan dataset logo yang di-crawling dari website LOGODIX.com.
- Training model dilakukan tanpa adanya dataset pre-processing yang memadai.
- Tingkat akurasi dari model hasil training masih terbilang rendah, sehingga diperlukan pengembangan lebih lanjut terkait penyempurnaan modelnya.

***Terima kasih 😃😃***